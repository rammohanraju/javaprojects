package com.ram.java.hackerearch;

import java.util.Scanner;


public class StringAnagram {
    public static int numberNeeded(String first, String second) {
        StringBuffer firstString = new StringBuffer(first);
        StringBuffer secondString = new StringBuffer(second);
        int i = 0;
        for (i = 0; i < firstString.length(); i++) {
            String matchChar = Character.toString(firstString.charAt(i));
            int secondStringMatchCharIndex = secondString.indexOf(matchChar);
            if (secondStringMatchCharIndex >= 0) {
                firstString.deleteCharAt(i);
                secondString.deleteCharAt(secondStringMatchCharIndex);
                i--;
            }
        }

        return firstString.length() + secondString.length();
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String a = in.next();
        String b = in.next();
        System.out.println(numberNeeded(a, b));
    }
}
