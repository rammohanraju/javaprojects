package com.ram.java.practice;

public class TestHashCode {
    public static void main(String[] args) {
        TestHashCode testHashCode = new TestHashCode();
        // testHashCode.testIntegerHashCode();
        testHashCode.testBooleanHashCode();
    }

    public void testIntegerHashCode() {
        Integer number = 10;
        System.out.println(number.hashCode());
    }

    public void testBooleanHashCode() {
        Boolean booleanValue = true;
        System.out.println(booleanValue.hashCode());
        System.out.println(Boolean.hashCode(true));
        System.out.println(Boolean.hashCode(false));
    }

}
