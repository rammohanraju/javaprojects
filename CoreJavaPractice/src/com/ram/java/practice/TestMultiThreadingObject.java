package com.ram.java.practice;

public class TestMultiThreadingObject {
    public static void main(String[] args) {
        System.out.println(Thread.currentThread().getName());
        try {
            new TestMultiThreadingObject().waitForSignal();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    void waitForSignal() throws InterruptedException {
        Object obj = new Object();
        synchronized (Thread.currentThread()) {
            obj.wait(); // java.lang.IllegerMonitorStateException
            obj.notify();
        }
    }
}
