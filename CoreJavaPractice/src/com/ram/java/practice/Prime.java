package com.ram.java.practice;

public class Prime {
    public static void main(String[] args) {
        int number = 13;
        boolean status = false;
        for (int i = 2; i < number / 2; i++) {
            if (number % i == 0) {
                status = false;
                break;
            }
            status = true;
        }
        System.out.println(number + " is prime ? : " + status);
    }
}
