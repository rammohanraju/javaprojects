package com.ram.java.practice;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


public class TestConcurrentMap {
    public static void main(String[] args) {
        ConcurrentMap<String, String> map = new ConcurrentHashMap<>();
        map.put("001", "test 001");
        map.put("002", "test 002");
        map.put("003", "test 003");
        map.putIfAbsent("001", "test 001 again");
        map.forEach((key, value) -> System.out.println(key + ": " + value));
    }
}
