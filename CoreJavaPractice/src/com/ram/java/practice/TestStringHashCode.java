package com.ram.java.practice;

public class TestStringHashCode {
    public static void main(String[] args) {
        String test = "Test";
        System.out.println("test  " + test.hashCode());

        String test2 = "Test";
        System.out.println("test2 " + test2.hashCode());
        String test3 = new String("Test");
        System.out.println("test3 " + test3.hashCode());
        String test4 = new String(test2);
        System.out.println("test4 " + test4.hashCode());
        String test5 = test2;
        System.out.println("test5 " + test5.hashCode());

        System.out.println(test == test2); // true
        System.out.println(test == test3); // false
        System.out.println(test == test4); // false

        System.out.println(test3 == test4);// false
        System.out.println(test2 == test5); // true

        test = "";
        System.out.println("test  " + test.hashCode());
        test = null;
        // System.out.println("test " + test.hashCode());
    }
}
