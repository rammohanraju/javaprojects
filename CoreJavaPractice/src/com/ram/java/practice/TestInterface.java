package com.ram.java.practice;

public interface TestInterface extends TestInterfaceBase {
    // void doSomething();

    @Override
    default void test() {
        System.out.println("I'm confused!");
    }
}
