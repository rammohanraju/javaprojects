package com.ram.java.practice;

public class ConstructorCallRecursive {
	public ConstructorCallRecursive() {
		System.out.println("Inside constructor");
		new ConstructorCallRecursive();
	}

	public static void main(String[] args) {
		ConstructorCallRecursive cons = new ConstructorCallRecursive();
	}
}
