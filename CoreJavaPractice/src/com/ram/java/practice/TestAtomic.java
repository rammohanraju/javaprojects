package com.ram.java.practice;

import java.util.concurrent.atomic.AtomicInteger;


public class TestAtomic {
    public static void main(String[] args) {
        AtomicInteger atomicInt = new AtomicInteger(10);
        System.out.println(atomicInt.incrementAndGet());
    }
}
