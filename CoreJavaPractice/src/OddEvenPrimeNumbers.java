import java.util.Scanner;


public class OddEvenPrimeNumbers {
    public static void main(String[] args) {
        System.out.println("Enter value for N: ");
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int sumOdd = 0, sumEven = 0, sumPrime = 0;
        for (int i = 1; i <= n; i++) {
            if (i % 2 == 0) {
                System.out.println(i + " is Even.");
                sumEven += i;
                if (i == 2) {
                    System.out.println(i + " is Prime.");
                    sumPrime += i;
                }
            } else {
                System.out.println(i + " is Odd.");
                sumOdd += i;
                if (i != 1 && isPrime(i)) {
                    System.out.println(i + " is Prime.");
                    sumPrime += i;
                }
            }
        }
        System.out.println("Sum of odd numbers: " + sumOdd);
        System.out.println("Sum of even numbers: " + sumEven);
        System.out.println("Sum of prime numbers: " + sumPrime);
    }

    private static boolean isPrime(int number) {
        for (int i = 2; i <= number / 2; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }
}
