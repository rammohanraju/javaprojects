import java.util.Scanner;
import java.util.stream.IntStream;;


public class StringRepeat {
    public static void main(String[] args) {
        System.out.println("Enter a string: ");
        Scanner scan = new Scanner(System.in);
        String inputString = scan.next();
        System.out.println(resolveStringRepeat(inputString));
    }

    private static String resolveStringRepeat(String inputString) {
        StringBuffer finalString = new StringBuffer();
        char[] chars = inputString.toCharArray();
        char currentChar = chars[0];
        int count = 1;
        for (char c : chars) {
            if (c != currentChar) {
                finalString.append(currentChar);
                finalString.append(count);
                count = 1;
                currentChar = c;
            } else {
                count++;
            }
        }
        finalString.append(currentChar);
        finalString.append(count);
        return finalString.toString();
    }
}
