/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ram.test.foreach;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author ram
 */
public class TestForEach {
   public void execute() {
       ArrayList<Integer> list = new ArrayList<>();
       Integer[] arr = {1,2,3,4,5,6};
       list.addAll(Arrays.asList(arr));
       list.forEach(item -> System.out.println(item));
    }
            
}
