/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ram.test;

import com.ram.test.foreach.TestForEach;
import com.ram.test.regex.RegexTestPatternMatcher;
import com.ram.test.regex.TestRegEx;

/**
 *
 * @author ram
 */
public class Java8Features {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //new TestForEach().execute();
        //new TestRegEx().execute();
        new RegexTestPatternMatcher().execute();
    }
    
}
