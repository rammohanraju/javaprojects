/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reversewordpuzzle;

/**
 *This is a java project for solving reverse word puzzle to find the minimum number of characters to insert to make the word equal from both sides. ex- level.
 * @author ram
 */
public class ReverseWordPuzzle {

     /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        ReverseWordPuzzle puzzle = new ReverseWordPuzzle();
        System.out.println(puzzle.solve(5, "sets"));
    }

    private int solve(int aNoOfLetters, String aWord) {
        if(isPalindromeWord(aWord)){
            return 0;
        }
        
        String repeatedChar = findRepeatedChar(aWord);
        System.out.print(repeatedChar);
        System.out.print(" : at index: ");
        System.out.println(aWord.indexOf(repeatedChar));
        return  -1;
    }

    private String findRepeatedChar(String aWord) {
       String repeatedChar = null;
       while(repeatedChar ==null && aWord != null && !aWord.isEmpty()){
           String firstChar = aWord.substring(0,1);
           aWord = aWord.substring(1, aWord.length());
           if(aWord.indexOf(firstChar) >=0){
               repeatedChar = firstChar;
           }
       }
       return repeatedChar;
    }

    private boolean isPalindromeWord(String aWord) {
        if(aWord.equals(new StringBuilder(aWord).reverse().toString())){
            return true;
        }
        return false;
    }
    
}
