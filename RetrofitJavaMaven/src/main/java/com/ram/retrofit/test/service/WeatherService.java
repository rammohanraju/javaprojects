package com.ram.retrofit.test.service;

import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Query;


public interface WeatherService {
    // @GET("/data/2.5/weather")
    // Weather getWeather(@Query("q") String q, @Query("appId") String appId);
    @GET("/data/2.5/weather")
    public Response getWeather(@Query("q") String q, @Query("appId") String appId);

    @GET("/data/2.5/weather")
    public void getWeather(@Query("q") String q, @Query("appId") String appId, retrofit.Callback<Response> callback);

}
