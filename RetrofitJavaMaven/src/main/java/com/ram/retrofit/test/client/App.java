package com.ram.retrofit.test.client;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ram.retrofit.test.constants.Constants;
import com.ram.retrofit.test.service.WeatherService;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;


public class App {

    public static void main(String[] args) {
        App app = new App();
        app.retrofitSynchronousCall();
        app.retrofitAsynchronousCall();
        // Weather w = service.getWeather("Rome,IT", "d5c178b1845c97b75872cd956ed306fc");
    }

    public void retrofitSynchronousCall() {
        Response response = getWeatherService().getWeather(Constants.CITY_STATE, Constants.APP_ID);

        // String output of the service response
        String responseFromService = new String(((TypedByteArray) response.getBody()).getBytes());
        System.out.println(responseFromService);

        // JSON output of the service response
        JsonObject jsonObject = new JsonParser().parse(new String(((TypedByteArray) response.getBody()).getBytes())).getAsJsonObject();
        System.out.println(jsonObject.toString());
    }

    public void retrofitAsynchronousCall() {
        getWeatherService().getWeather(Constants.CITY_STATE, Constants.APP_ID, new Callback<Response>() {

            @Override
            public void failure(RetrofitError arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void success(Response result, Response response) {
                System.out.println(new String(((TypedByteArray) response.getBody()).getBytes()));

            }
        });
    }

    public RestAdapter getRestAdapter() {
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(Constants.WEATHER_SERVICE_ENDPOINT).build();
        return restAdapter;
    }

    public WeatherService getWeatherService() {
        WeatherService weatherService = getRestAdapter().create(WeatherService.class);
        return weatherService;
    }

}
