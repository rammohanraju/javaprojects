package com.test.pojos;

import java.util.List;


public class Weather {
    Coord coord;
    Sys sys;
    List<WeatherInfo> weather;
    WeatherMain main;
    Wind wind;
    Clouds clouds;

    int id;
    String name;
    String cod;
    String dt;

    @Override
    public String toString() {
        return "Weather{\n " + "coord=" + coord + ",\n sys=" + sys + ",\n weather=" + weather + ",\n main=" + main + ",\n wind=" + wind
                        + ",\n clouds=" + clouds + ",\n id=" + id + ",\n name=" + name + ",\n cod=" + cod + ",\n dt=" + dt + '}';
    }

}
