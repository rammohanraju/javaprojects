package com.ram.java.gfgamazon;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class KLargestElements {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int noOfTestCases = scanner.nextInt();
        while (noOfTestCases-- > 0) {
            int noOfElements = scanner.nextInt();
            int noOfKLargestElements = scanner.nextInt();
            int elements[] = new int[noOfElements];
            for (int i = 0; i < noOfElements; i++) {
                elements[i] = scanner.nextInt();
            }
            //getSortedArray(elements);
            Arrays.sort(elements);
            

            printElements(elements);
        
        }
        scanner.close();
    }
    
    private static void printElements(final int[] aElements){
        StringBuilder elementsStr = new StringBuilder();
        for(int i : aElements){
            elementsStr.append(i);
            elementsStr.append(" ");
        }
        System.out.println(elementsStr.toString().trim());
    }
    
//    private static int[] getSortedArray(final int[] aElementsArray){
//        int tempElements[] = new int[aElementsArray.length];
//        
//        return aElementsArray;
//    }
}
